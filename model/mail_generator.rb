require 'mail'
require 'email_address'
require_relative 'template_interpreter'

BAD_EMAIL_ADDRESS_MESSAGE = "direccion de mail invalida"

class BadEmailAddressError < StandardError
  attr_reader :error_msg
  def initialize
    @error_msg = BAD_EMAIL_ADDRESS_MESSAGE
    super(error_msg)
  end
end

# Generates a Mail using the data provided.
class MailGenerator

  def from_hash(mail_info)
    # Generates a Mail from the hash provided.
    # The hash should have been generated from a valid JSON.
    # The mail is ready to be sent.
    raise BadEmailAddressError unless EmailAddress.valid?(mail_info[:mail])
    mail = Mail.new
    mail[:to] = mail_info[:mail]
    mail[:from] = mail_info[:sender_info][:remitente]
    mail[:subject] = mail_info[:sender_info][:asunto]


    mail[:body] = TemplateInterpreter.new.interpret(mail_info[:template],
                                                    mail_info[:sendee_info],
                                                    mail_info[:sender_info])
    mail
  end

end
