require 'open-uri'
require 'easy_translate'
require 'nokogiri'

class TemplateInterpreter
  DELIMITER = '%%'.freeze
  PLUS_GET_TITLE_TEXT_XPATH = '%%//title/text()'.freeze

  INNER_TAG_REGEXP = /(<([^><]*)>)/
  OUTTER_PARENS_REGEXP = /\((?>[^)(]+|\g<0>)*\)/

  def interpret(base_template, sendee_info, sender_info)
    # Receives a hash of hashes with keys {contact_info, sender_info, template}
    # Returns the interpreted template, or raises a FIXME exception
    aux = String.new(base_template)
    while aux =~ INNER_TAG_REGEXP
      aux.match(INNER_TAG_REGEXP) do |match|
        processor = get_tag_name(match[0])
        aux.sub!(match[0], send(processor, match[2], sendee_info, sender_info))
      end
    end
    aux
  end

  private

  def if(ocurrence, sendee_info, sender_info)
    # Processor for If tags.
    aux = get_arg(ocurrence)
    static_var = static(aux.split(DELIMITER)[0].split(' = ')[0],
                        sendee_info, sender_info)
    target_var = aux.split(DELIMITER)[0].split(' = ')[1]
    true_outcome, false_outcome = aux.split(DELIMITER)[1..-1]
    static_var == target_var ? true_outcome : false_outcome
  end

  def title(ocurrence, sendee_info, sender_info)
    # Processor for Title tags.
    target_url = get_arg(ocurrence)
    built_xpath = '(' + target_url + PLUS_GET_TITLE_TEXT_XPATH + ')'
    xpath(built_xpath, sendee_info, sender_info)
  end

  def xpath(ocurrence, _sendee_info, _sender_info)
    # Processor for Xpath tags.
    target_url = get_arg(ocurrence).split(DELIMITER)[0]
    path = get_arg(ocurrence).split(DELIMITER)[1]
    Nokogiri::XML(open(target_url)).xpath(path).text
  end

  def translate(ocurrence, _seende_info, _sender_info)
    # Processor for Translate tags.
    target_lang = get_arg(ocurrence).split(DELIMITER)[0].to_sym
    text = get_arg(ocurrence).split(DELIMITER)[1]
    EasyTranslate.translate(text, from: :spanish, to: target_lang).downcase
  end

  def embed(ocurrence, _sendee_info, _sender_info)
    # Processor for Embed tags.
    target_url = str_between(ocurrence, '(', ')')
    uri = URI.parse(target_url)
    uri.read
  end

  def today(ocurrence, _sendee_info, _sender_info)
    # Processor for Today tags.
    requested_format = str_between(ocurrence, '(', ')')
    DateTime.now.strftime(requested_format)
  end

  def empty(ocurrence, sendee_info, sender_info)
    # Processor for Empty tags.
    empty_vars = get_arg(ocurrence)
    empty_field = empty_vars.split(DELIMITER)[0].to_sym
    empty_default = empty_vars.split(DELIMITER)[1]
    begin
      sendee_info[empty_field] || sender_info.fetch(empty_field)
    rescue KeyError
      empty_default
    end
  end

  def static(ocurrence, sendee_info, sender_info)
    # Processor for Static tags.
    static_variable_name = ocurrence
    static_symbol = static_variable_name.to_sym
    sendee_info[static_symbol] || sender_info.fetch(static_symbol)
  end

  def get_tag_name(tag)
    # Returns the name of the received tag.
    aux = str_between(tag, '<', '(')
    return :static if aux.nil?
    aux.to_sym
  end

  def str_between(str, s_start, s_end)
    # Returns the string between the delimiters received.
    str[/#{Regexp.escape(s_start)}(.*?)#{Regexp.escape(s_end)}/m, 1]
  end

  def get_arg(str)
    # Returns the argument of a tag.
    str.match(OUTTER_PARENS_REGEXP)[0][1..-2]
  end
end
