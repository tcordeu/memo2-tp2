require 'json-schema'

REQUEST_SCHEMA = {
  "type" => "object",
  "properties" => {
    "debug" => {"type" => "boolean"},
    "contactos" => {"type" => "array",
                    "items" => {"type" => "object"},
                   },
    "datos" => {"type" => "object"},
    "template" => {"type" => "string"}
  },
  "required" => ["contactos","datos", "template"]
}

class SchemaValidator

  def valid_request?(request)
    JSON::Validator.validate(REQUEST_SCHEMA, request)
  end
end
