require_relative 'mail_generator'

ERROR_MESSAGE_MISSING_KEY_BASE = "falta etiqueta %s"
ERROR_MESSAGE_URL_UNREACHABLE = "URL no encontrada"
ERROR_MESSAGE_URL_GIVES_ERROR = "Error al procesar URL"
ERROR_MESSAGE_TRANSLATE = "Error al intentar traducir"
ERROR_MESSAGE_INVALID_XPATH = "expresion xpath invalida"

class ProcessingErrorHandler

  def with_error_catching_and_pushing(error_messages_list)
    yield
  rescue KeyError => e
    missing_key = e.to_s.match(/key not found: \:(.*)/) { $1 }
    error_messages_list.push(ERROR_MESSAGE_MISSING_KEY_BASE % missing_key)
  rescue BadEmailAddressError => e
    error_message = e.error_msg
    error_messages_list.push(error_message)
  rescue SocketError
    error_messages_list.push(ERROR_MESSAGE_URL_UNREACHABLE)
  rescue OpenURI::HTTPError
    error_messages_list.push(ERROR_MESSAGE_URL_GIVES_ERROR)
  rescue EasyTranslate::EasyTranslateException
    error_messages_list.push(ERROR_MESSAGE_TRANSLATE)
  rescue Nokogiri::XML::XPath::SyntaxError
    error_messages_list.push(ERROR_MESSAGE_INVALID_XPATH)
  end
end
