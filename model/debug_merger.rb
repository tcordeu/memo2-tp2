require 'json'
require_relative 'mail_generator'

class DebugMerger

  def initialize
    @error_pusher = ProcessingErrorHandler.new
  end

  def merge(mails_info)
    @errors = []
    mail = @error_pusher.with_error_catching_and_pushing(@errors) do
      MailGenerator.new.from_hash(mails_info.first)
    end
    [ mail, @errors ]
  end

end
