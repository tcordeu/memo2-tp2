require 'json'
require_relative 'mail_generator'
require_relative 'processing_error_handler'

class StandardMerger

  def initialize
    @error_pusher = ProcessingErrorHandler.new
  end

  def merge(mails_info)
    @errors = []
    @mails = []
    mails_info.each do |mail_info|
      @error_pusher.with_error_catching_and_pushing(@errors) do
        mail = MailGenerator.new.from_hash(mail_info)
        @mails.push(mail)
        send_mail(mail)
      end
    end
    [ @mails, @errors ]
  end

  private

  def send_mail(mail)
    mail.deliver!
  end
end
