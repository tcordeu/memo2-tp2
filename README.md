# TP2 TDD/MeMo2 (1/2018): Mail Merge

## How to's

### Para instalar las gemas
~~~
bundle install
~~~

### Para correr las pruebas
~~~
bundle exec rake
~~~

### Para correr el servidor localmente.
~~~
bundle exec ruby app.rb
~~~

### Para recibir mails localmente.
~~~
mailcatcher
~~~
