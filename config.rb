if Sinatra::Base.environment == :production
  # Configuration for SendGrid in production.
  Mail.defaults do
    delivery_method :smtp, address: 'smtp.sendgrid.net',
                           port: 587,
                           user_name: ENV['SENDGRID_LOGIN'],
                           password: ENV['SENDGRID_PASSWORD'],
                           authentication: 'plain'
  end
  # Configuration for Google Translate API.
  EasyTranslate.api_key = ENV['TRANSLATE_KEY']
elsif Sinatra::Base.environment == :development
  # Configuration for Mailcatcher in development.
  Mail.defaults do
    delivery_method :smtp, address: 'localhost', port: 1025
  end
  # Configuration for Google Translate API.
  EasyTranslate.api_key = ENV['TRANSLATE_KEY']
end
