require 'sinatra'
require 'json'
require_relative 'model/schema_validator'
require_relative 'model/mail_generator'
require_relative 'config'
require_relative 'model/debug_merger'
require_relative 'model/standard_merger'

BAD_REQUEST_STATUS_CODE = 400
GOOD_REQUEST_STATUS_CODE = 200
ACCEPTED_STATUS_CODE = 202

ERROR_MESSAGE_NOT_JSON = 'entrada no json'
ERROR_MESSAGE_BAD_SCHEMA = "json con esquema incorrecto"

helpers do
  def debug_on?(rcv_json)
    rcv_json.key?(:debug) && rcv_json[:debug] == true
  end

  def with_json_parsing_error_handling
    yield
  rescue JSON::ParserError
    halt BAD_REQUEST_STATUS_CODE, {msg_ok: 0, error: ERROR_MESSAGE_NOT_JSON}.to_json
  end

  def valid_schema?(request_body)
    SchemaValidator.new.valid_request?(request_body)
  end

  def content_to_mails_info(content)
    mails_info = []
    content[:contactos].each do |contact_info|
      new_mail_info = Hash[template: content[:template],
                           sender_info: content[:datos],
                           sendee_info: contact_info,
                           mail: contact_info[:mail]]
      mails_info.push(new_mail_info)
    end
    mails_info
  end

  def status_code_from_successes(number_of_successes, number_of_cases)
    case number_of_successes
    when 0 then BAD_REQUEST_STATUS_CODE
    when number_of_cases then GOOD_REQUEST_STATUS_CODE
    else ACCEPTED_STATUS_CODE
    end
  end
end

post '/' do
  content = with_json_parsing_error_handling { JSON.parse(request.body.read, symbolize_names: true) }
  content_type :json

  if !valid_schema?(content)
    halt 400, !debug_on?(content) ? {msg_ok: 0, error: ERROR_MESSAGE_BAD_SCHEMA}.to_json : {error: ERROR_MESSAGE_BAD_SCHEMA}.to_json
  end

  mails_info = content_to_mails_info(content)

  if debug_on?(content)
    mail, error_list = DebugMerger.new.merge(mails_info)
    if error_list.empty?
      { text: mail.body.to_s }.to_json
    else
      [BAD_REQUEST_STATUS_CODE, { error: error_list.first }.to_json]
    end
  else
    mails, error_list = StandardMerger.new.merge(mails_info)
    ret_status = status_code_from_successes(mails.size, mails_info.size)
    [ret_status, { msg_ok: mails.size }.to_json]
  end
end
