require_relative '../model/schema_validator'

describe SchemaValidator do

  let (:validator) { SchemaValidator.new }

  it 'complete request, including debug field is a valid request' do
    request = {
      debug: true,
      template: "test template",
      contactos: [{"mail":"juanperez@test.com"}],
      datos: {remitente: "pepe@test.com",
              asunto: "hola"}
    }.to_json

    expect(validator.valid_request?(request)).to eq true
  end

  it 'request lacking debug field is a valid request' do
    request = {
      template: "test template",
      contactos: [{"mail":"juanperez@test.com"}],
      datos: {remitente: "pepe@test.com",
              asunto: "hola"}
    }.to_json

    expect(validator.valid_request?(request)).to eq true
  end

  it 'request lacking "template" field is NOT a valid request' do
    request = {
      debug: true,
      contactos: [{"mail":"juanperez@test.com"}],
      datos: {remitente: "pepe@test.com",
              asunto: "hola"}
    }.to_json

    expect(validator.valid_request?(request)).to eq false
  end

  it 'request lacking "contactos" field is NOT a valid request' do
    request = {
      debug: true,
      template: "test template",
      datos: {remitente: "pepe@test.com",
              asunto: "hola"}
    }.to_json

    expect(validator.valid_request?(request)).to eq false
  end

  it 'request lacking "datos" field is NOT a valid request' do
    request = {
      debug: true,
      template: "test template",
      contactos: [{"mail":"juanperez@test.com"}]
    }.to_json

    expect(validator.valid_request?(request)).to eq false
  end
end
