require_relative('../model/debug_merger.rb')

describe 'Debug Merger' do
  let(:merger) { DebugMerger.new }

  def _make_mail_info(base_template, sendee_info, sender_info)
    Hash[template: base_template,
         sender_info: sender_info,
         sendee_info: sendee_info,
         mail: sendee_info[:mail]]
  end

  it 'merge of a list with one ok mail info with base template "hola mundo" should return'+
  ' a mail with body "hola mundo" and an empty error list' do
    in_base_template =  "hola mundo"
    in_sendee_info =  { mail: 'juanperez@test.com' }
    in_sender_info = { asunto: 'Saludo', remitente: 'pepe@test.com' }
    in_mail_info = [_make_mail_info(in_base_template, in_sendee_info, in_sender_info)]

    expected_mail_body = "hola mundo"

    resulting_mail, resulting_error_list = merger.merge(in_mail_info)
    expect(resulting_mail.body.to_s).to eq expected_mail_body
    expect(resulting_error_list).to be_empty
  end

  it 'merge of a list with two ok mail infos and first has base template "hola mundo"'+
  ' should return a mail with body "hola mundo" and an empty error list' do
    in_base_template1 =  "hola mundo"
    in_sendee_info1 =  { mail: 'juanperez@test.com' }
    in_sender_info1 = { asunto: 'Saludo', remitente: 'pepe@test.com' }

    in_base_template2 =  "asdasdad"
    in_sendee_info2 =  { mail: 'zaraza' }
    in_sender_info2 = { asunto: 'Saludo', remitente: 'pepe@test.com' }

    in_mail_info = [_make_mail_info(in_base_template1, in_sendee_info1, in_sender_info1),
                    _make_mail_info(in_base_template2, in_sendee_info2, in_sender_info2)]

    expected_mail_body = "hola mundo"

    resulting_mail, resulting_error_list = merger.merge(in_mail_info)
    expect(resulting_mail.body.to_s).to eq expected_mail_body
    expect(resulting_error_list).to be_empty
  end

  it 'merge of a list with first mail info lacking static tag "pepe" should'+
  ' return an error list with 1 error message' do
    in_base_template =  "hola <pepe>"
    in_sendee_info =  { mail: 'juanperez@test.com' }
    in_sender_info = { asunto: 'Saludo', remitente: 'pepe@test.com' }
    in_mail_info = [_make_mail_info(in_base_template, in_sendee_info, in_sender_info)]

    expected_error_list_size = 1

    resulting_mail, resulting_error_list = merger.merge(in_mail_info)
    expect(resulting_error_list.size).to eq expected_error_list_size
  end

  it 'merge of a list with first mail trying to embed a http-error-raising URL should'+
  ' return an error list with 1 error message' do
    in_base_template =  "asd <embed(some_url)>"
    in_sendee_info =  { mail: 'juanperez@test.com' }
    in_sender_info = { asunto: 'Saludo', remitente: 'pepe@test.com' }
    in_mail_info = [_make_mail_info(in_base_template, in_sendee_info, in_sender_info)]

    TemplateInterpreter.any_instance.stub(:embed).and_raise(OpenURI::HTTPError.new("hola","someIO"))

    expected_error_list_size = 1

    resulting_mail, resulting_error_list = merger.merge(in_mail_info)
    expect(resulting_error_list.size).to eq expected_error_list_size
  end

  it 'merge of a list with first mail looking for xpath //title/text() in job vacancy URL '+
  'should return 1 mail with body "titulo es Job Vacancy Board - find the best jobs"' do
    in_base_template =  "titulo es <xpath(some_url%%//title/text())>"
    in_sendee_info =  { mail: 'juanperez@test.com' }
    in_sender_info = { asunto: 'Saludo', remitente: 'pepe@test.com' }
    in_mail_info = [_make_mail_info(in_base_template, in_sendee_info, in_sender_info)]

    TemplateInterpreter.any_instance.stub(:open).and_return(File.open(JOB_VACANCY_FILE_PATH))

    expected_mail_body = "titulo es Job Vacancy Board - find the best jobs"

    resulting_mail, resulting_error_list = merger.merge(in_mail_info)
    expect(resulting_mail.body.to_s).to eq expected_mail_body
    expect(resulting_error_list).to be_empty
  end

  it 'merge of a list with first mail looking for title of job vacancy URL should '+
  'return 1 mail with body "titulo es Job Vacancy Board - find the best jobs" and no errors' do
    in_base_template =  "titulo es <title(some_url)>"
    in_sendee_info =  { mail: 'juanperez@test.com' }
    in_sender_info = { asunto: 'Saludo', remitente: 'pepe@test.com' }
    in_mail_info = [_make_mail_info(in_base_template, in_sendee_info, in_sender_info)]

    TemplateInterpreter.any_instance.stub(:open).and_return(File.open(JOB_VACANCY_FILE_PATH))

    expected_mail_body = "titulo es Job Vacancy Board - find the best jobs"

    resulting_mail, resulting_error_list = merger.merge(in_mail_info)
    expect(resulting_mail.body.to_s).to eq expected_mail_body
    expect(resulting_error_list).to be_empty
  end
end
