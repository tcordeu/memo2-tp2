# spec/mail_generator_spec.rb

require File.expand_path 'spec_helper.rb', __dir__
require_relative('../model/mail_generator.rb')

describe 'Mail generator' do
  let(:mail_gen) { MailGenerator.new }

  it 'generate mail with literal template for one valid contact' do
    data = {
      mail: 'juanperez@test.com',
      template: 'Hola Mundo!',
      sendee_info: { mail: 'juanperez@test.com' },
      sender_info: { asunto: 'Saludo', remitente: 'pepe@test.com' }
    }

    mail = mail_gen.from_hash(data)

    expect(mail[:to].to_s).to eq data[:mail]
    expect(mail.body.to_s).to eq data[:template]
    expect(mail[:from].to_s).to eq data[:sender_info][:remitente]
    expect(mail[:subject].to_s).to eq data[:sender_info][:asunto]
  end

  it 'generate mail with literal template for one invalid contact' do
    data = {
      mail: 'juanperez',
      template: 'Hola Mundo!',
      sendee_info: { mail: 'juanperez' },
      sender_info: { asunto: 'Saludo', remitente: 'pepe@test.com' }
    }

    expect{mail_gen.from_hash(data)}.to raise_error(BadEmailAddressError)
  end

  it 'generate mail with static variable in contact info' do
    data = {
      mail: "susana_the_g@test.com",
      template: "Hola <nombre>",
      sendee_info: {mail:"susana_the_g@test.com",
                    nombre: "Susana"},
      sender_info: {remitente: "pepe@test.com",
                    asunto: "hola susana"}
    }
    expected_body = "Hola Susana"

    mail = mail_gen.from_hash(data)
    expect(mail.body.to_s).to eq expected_body
  end

end
