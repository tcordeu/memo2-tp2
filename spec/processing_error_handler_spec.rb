require_relative '../model/processing_error_handler.rb'

describe 'Processing Error Catcher' do

  let (:catcher) { ProcessingErrorHandler.new }

  before(:all) do
    @unreachable_url = "http://www.nonreachable.com/"
  end

  it 'Catching no errors should keep an empty list empty' do
    error_list = []

    catcher.with_error_catching_and_pushing(error_list) { x = 2 }

    expect(error_list).to be_empty
  end

  it 'Catching a missing :hola key error should add "falta etiqueta hola" message to error list' do
    error_list = []

    catcher.with_error_catching_and_pushing(error_list) { Hash.new.fetch(:hola)}

    expect(error_list.first).to eq "falta etiqueta hola"
  end

  it 'Catching a socket error should add "URL no encontrada" message to error list' do
    error_list = []

    catcher.with_error_catching_and_pushing(error_list) { URI.parse(@unreachable_url).read }

    expect(error_list.first).to eq ERROR_MESSAGE_URL_UNREACHABLE
  end

  it 'Catching an http error should add "Error al procesar URL" message to error list' do
    error_list = []

    catcher.with_error_catching_and_pushing(error_list) { raise OpenURI::HTTPError.new("","") }

    expect(error_list.first).to eq ERROR_MESSAGE_URL_GIVES_ERROR
  end

  it 'Catching a xml xpath syntax error should add "expresion xpath invalida" message to error list' do
    error_list = []

    catcher.with_error_catching_and_pushing(error_list) { raise Nokogiri::XML::XPath::SyntaxError }

    expect(error_list.first).to eq ERROR_MESSAGE_INVALID_XPATH
  end
end
