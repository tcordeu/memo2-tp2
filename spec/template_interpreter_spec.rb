require_relative('../model/template_interpreter.rb')

describe 'Template interpreter' do
  let(:interpreter) { TemplateInterpreter.new }

  before(:each) do
    @fake_day = 01
    @fake_month = 02
    @fake_year = 2018
    DateTime.stub(:now).and_return(DateTime.new(@fake_year, @fake_month, @fake_day))
    @fake_embedding = "grillo"
    interpreter.stub(:embed).and_return(@fake_embedding)
    @fake_translate = "car"
    interpreter.stub(:translate).and_return(@fake_translate)
    interpreter.stub(:open).and_return(File.open(JOB_VACANCY_FILE_PATH))
  end

  it 'interpretation of literal template "asd dsa" gives the same string literal' do
    in_base_template =  "asd dsa"
    in_sendee_info =  { mail: 'juanperez@test.com' }
    in_sender_info = { asunto: 'Saludo', remitente: 'pepe@test.com' }

    expected_output = in_base_template

    expect(interpreter.interpret(in_base_template, in_sendee_info, in_sender_info)).to eq expected_output
  end

  it 'interpretation of "hola <nombre>" with "nombre":"juan" in sendee_info gives "hola juan"' do
    in_base_template =  "hola <nombre>"
    in_sendee_info =  { mail: 'juanperez@test.com', nombre: "juan" }
    in_sender_info = { asunto: 'Saludo', remitente: 'pepe@test.com' }

    expected_output = "hola juan"

    expect(interpreter.interpret(in_base_template, in_sendee_info, in_sender_info)).to eq expected_output
  end

  it 'interpretation of "hola <apodo>" with "apodo":"don pepito" in sendee_info gives "hola don pepito"' do
    in_base_template =  "hola <apodo>"
    in_sendee_info =  { mail: 'juanperez@test.com', apodo:"don pepito" }
    in_sender_info = { asunto: 'Saludo', remitente: 'pepe@test.com' }

    expected_output = "hola don pepito"

    expect(interpreter.interpret(in_base_template, in_sendee_info, in_sender_info)).to eq expected_output
  end

  it 'trying to interpret "hola <nombre>" when <nombre> is not included raises KeyError' do
    in_base_template =  "hola <name>"
    in_sendee_info =  { mail: 'juanperez@test.com' }
    in_sender_info = { asunto: 'Saludo', remitente: 'pepe@test.com' }

    expect{interpreter.interpret(in_base_template, in_sendee_info, in_sender_info)}.to raise_error(KeyError)
  end

  it 'interpretation of "hola <nombre>" with "nombre":"juan" in sender_info gives "hola juan"' do
    in_base_template =  "hola <nombre>"
    in_sendee_info =  { mail: 'juanperez@test.com'}
    in_sender_info = { asunto: 'Saludo', remitente: 'pepe@test.com', nombre: "juan" }

    expected_output = "hola juan"

    expect(interpreter.interpret(in_base_template, in_sendee_info, in_sender_info)).to eq expected_output
  end

  it 'interpretation of "Hola <empty(nombre%%pibe)>" gives "Hola pibe"' do
    in_base_template = "Hola <empty(nombre%%pibe)>"
    in_sendee_info = { "mail": "juanperez@test.com" }
    in_sender_info = { "asunto": "Saludo", "remitente": "pepe@test.com"}
    expected_output = "Hola pibe"

    expect(interpreter.interpret(in_base_template, in_sendee_info, in_sender_info)).to eq expected_output
  end

  it 'interpretation of "Hola <nombre> <empty(apodo%%pibe)>" gives "Hola Pepe pibe"' do
    in_base_template = "Hola <nombre> <empty(apodo%%pibe)>"
    in_sendee_info = { "mail": "juanperez@test.com", "nombre": "Pepe" }
    in_sender_info = { "asunto": "Saludo", "remitente": "pepe@test.com"}
    expected_output = "Hola Pepe pibe"

    expect(interpreter.interpret(in_base_template, in_sendee_info, in_sender_info)).to eq expected_output
  end

  it 'interpretation of "Hola <empty(nombre%%pibe)>" with nombre in datos gives "Hola Juan' do
    in_base_template = "Hola <empty(nombre%%pibe)>"
    in_sendee_info = { "mail": "juanperez@test.com"}
    in_sender_info = { "asunto": "Saludo", "remitente": "pepe@test.com", "nombre": "Juan"}
    expected_output = "Hola Juan"

    expect(interpreter.interpret(in_base_template, in_sendee_info, in_sender_info)).to eq expected_output
  end

  it 'intepretation of "hola <today(%d%m%Y)>" gives "hola 01022018"' do
    in_base_template = "hola <today(%d%m%Y)>"
    in_sendee_info = { "mail": "juanperez@test.com"}
    in_sender_info = { "asunto": "Saludo", "remitente": "pepe@test.com"}
    expected_output = "hola 01022018"

    expect(interpreter.interpret(in_base_template, in_sendee_info, in_sender_info)).to eq expected_output
  end

  it 'intepretation of "hola <today(%Y%d%m)>" gives "hola 20180102"' do
    in_base_template = "hola <today(%Y%d%m)>"
    in_sendee_info = { "mail": "juanperez@test.com"}
    in_sender_info = { "asunto": "Saludo", "remitente": "pepe@test.com"}
    expected_output = "hola 20180102"

    expect(interpreter.interpret(in_base_template, in_sendee_info, in_sender_info)).to eq expected_output
  end

  it 'interpretation of "pepe <embed(SOME_URL)>" when SOME_URL has content "hola\n"
  gives "pepe hola\n"' do
    embedding_url = "https://raw.githubusercontent.com/nicopaez/ejemplo/master/hola.txt"
    in_base_template = "pepe <embed(%s)>" % embedding_url
    in_sendee_info = { "mail": "juanperez@test.com"}
    in_sender_info = { "asunto": "Saludo", "remitente": "pepe@test.com"}
    expected_output = "pepe "+@fake_embedding

    expect(interpreter.interpret(in_base_template, in_sendee_info, in_sender_info)).to eq expected_output
  end

  it 'interpretation of "auto en ingles es <translate(EN%%auto)>" should give "auto en ingles
      es car"' do
    in_base_template = "auto en ingles es <translate(EN%%auto)>"
    in_sendee_info = { "mail": "juanperez@test.com"}
    in_sender_info = { "asunto": "Saludo", "remitente": "pepe@test.com"}
    expected_output = "auto en ingles es "+ @fake_translate

    expect(interpreter.interpret(in_base_template, in_sendee_info, in_sender_info)).to eq expected_output
  end

  it 'interpretation of "titulo es <xpath(some_url%%//title/text())>" when some_url has title "Job'+
  ' Vacancy Board - find the best jobs" should give "titulo es Job Vacancy Board - find the best jobs"' do
    in_base_template = "titulo es <xpath(http://www.jobvacancy.com.ar%%//title/text())>"
    in_sendee_info = { "mail": "juanperez@test.com"}
    in_sender_info = { "asunto": "Saludo", "remitente": "pepe@test.com"}
    expected_output = "titulo es Job Vacancy Board - find the best jobs"

    expect(interpreter.interpret(in_base_template, in_sendee_info, in_sender_info)).to eq expected_output
  end

  it 'interpretation of "titulo es <title(some_url)>" when some_url has title "Job Vacancy'+
  'Board - find the best jobs" should give "titulo es Job Vacancy Board - find the best jobs"' do
    in_base_template = "titulo es <title(http://www.jobvacancy.com.ar)>"
    in_sendee_info = { "mail": "juanperez@test.com"}
    in_sender_info = { "asunto": "Saludo", "remitente": "pepe@test.com"}
    expected_output = "titulo es Job Vacancy Board - find the best jobs"

    expect(interpreter.interpret(in_base_template, in_sendee_info, in_sender_info)).to eq expected_output
  end

  it 'interpretation of "Hey <if(pais = argentina%%hola che%%hello)>" when pais is not argentina should
      return "Hey hello"' do
    in_base_template = "Hey <if(pais = argentina%%hola che%%hello)>"
    in_sendee_info = { "mail": "juanperez@test.com"}
    in_sender_info = { "asunto": "Saludo", "remitente": "pepe@test.com", "pais": "dinamarca"}
    expected_output = "Hey hello"

    expect(interpreter.interpret(in_base_template, in_sendee_info, in_sender_info)).to eq expected_output
  end

  it 'interpretation of "Hey <if(pais = argentina%%hola che%%hello)>" when pais is argentina should
      return "Hey hola che"' do
    in_base_template = "Hey <if(pais = argentina%%hola che%%hello)>"
    in_sendee_info = { "mail": "juanperez@test.com"}
    in_sender_info = { "asunto": "Saludo", "remitente": "pepe@test.com", "pais": "argentina"}
    expected_output = "Hey hola che"

    expect(interpreter.interpret(in_base_template, in_sendee_info, in_sender_info)).to eq expected_output
  end

    it 'interpretation of "Hola <empty(fecha%%<today(%d%m%Y)>)> gives "Hola 01022018"' do
    in_base_template = "Hola <empty(fecha%%<today(%d%m%Y)>)>"
    in_sendee_info = { "mail": "juanperez@test.com"}
    in_sender_info = { "asunto": "Saludo", "remitente": "pepe@test.com"}
    expected_output = "Hola 01022018"

    expect(interpreter.interpret(in_base_template, in_sendee_info, in_sender_info)).to eq expected_output
  end

  it 'interpretation of "Hola <today(<empty(formato%%nil)>)> gives "Hola 20180201"' do
    in_base_template = "Hola <today(<empty(formato%%nil)>)>"
    in_sendee_info = { "mail": "juanperez@test.com"}
    in_sender_info = { "asunto": "Saludo", "remitente": "pepe@test.com", "formato": "%Y%m%d"}
    expected_output = "Hola 20180201"

    expect(interpreter.interpret(in_base_template, in_sendee_info, in_sender_info)).to eq expected_output
  end

  it 'interpreatation of "Hola <empty(feriado%%<if(pais = argentina%%siempre%%nunca)>)>" gives "Hola 9 de julio"' do
    in_base_template = "Hola <empty(feriado%%<if(pais = argentina%%siempre%%nunca)>)>"
    in_sendee_info = {"mail":"juanperez@test.com", "nombre":"Juan"}
    in_sender_info =  {"asunto":"caso e9","remitente":"pepe@test.com", "pais":"argentina", "feriado":"9 de julio"}
    expected_output = "Hola 9 de julio"

    expect(interpreter.interpret(in_base_template, in_sendee_info, in_sender_info)).to eq expected_output
  end

  it 'interpreatation of "Hola <empty(feriado%%<if(pais = argentina%%siempre%%nunca)>)>" gives "Hola siempre"' do
    in_base_template = "Hola <empty(feriado%%<if(pais = argentina%%siempre%%nunca)>)>"
    in_sendee_info = {"mail":"juanperez@test.com", "nombre":"Juan"}
    in_sender_info =  {"asunto":"caso e9","remitente":"pepe@test.com", "pais":"argentina"}
    expected_output = "Hola siempre"

    expect(interpreter.interpret(in_base_template, in_sendee_info, in_sender_info)).to eq expected_output
  end
end
