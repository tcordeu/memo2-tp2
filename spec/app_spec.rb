# spec/app_spec.rb

require File.expand_path 'spec_helper.rb', __dir__

describe 'Sinatra Application' do
  before(:all) do
    @ok_json = {
      msg_ok: 1
    }.to_json
  end

  before(:each) do
    StandardMerger.any_instance.stub(:send_mail).and_return(nil)
    @fake_day = 01
    @fake_month = 01
    @fake_year = 2018
    DateTime.stub(:now).and_return(DateTime.new(@fake_year, @fake_month, @fake_day))
    @fake_embedding = "hola/n"
    TemplateInterpreter.any_instance.stub(:embed).and_return(@fake_embedding)
    @fake_translate = "hello"
    TemplateInterpreter.any_instance.stub(:translate).and_return(@fake_translate)
    TemplateInterpreter.any_instance.stub(:open).and_return(File.open(JOB_VACANCY_FILE_PATH))
  end

  def _post(post_json)
    post '/', post_json, { 'CONTENT_TYPE' => 'application/json',
                           'ACCEPT' => 'application/json' }
  end

  it 'post / with implicit debug should return json {"msg_ok": 1}' do
    post_json = {
      template: "Hola Mundo",
      contactos: [{"mail":"juanperez@test.com"}],
      datos: {remitente: "pepe@test.com",
              asunto: "hola"}
    }.to_json

    _post(post_json)

    expect(last_response).to be_ok
    expect(last_response.body).to eq @ok_json
  end

  it 'post / with debug=false should return json {"msg_ok: 1}' do
    post_json = {
      debug: false,
      template: "test template",
      contactos: [{"mail":"juanperez@test.com"}],
      datos: {remitente: "pepe@test.com",
              asunto: "hola"}
    }.to_json

    _post(post_json)

    expect(last_response).to be_ok
    expect(last_response.body).to eq @ok_json
  end

  it 'post / with debug=true should return json {"text": <template>}' do
    post_json = {
      debug: true,
      template: "test template",
      contactos: [{"mail":"juanperez@test.com"}],
      datos: {remitente: "pepe@test.com",
              asunto: "hola"}
    }.to_json
    expected_json = { text: "test template" }.to_json

    _post(post_json)

    expect(last_response).to be_ok
    expect(last_response.body).to eq expected_json
  end

  it 'post / with non-json body should return bad request (400) status code' do
    post_not_json = "not_json_lol"

    post '/', post_not_json

    expect(last_response.status).to eq BAD_REQUEST_STATUS_CODE
  end

  it 'post / with non-json body should return json {"msg_ok":0,"error":"entrada no json"}' do
    post_not_json = "not_json_lol"
    expected_json = {msg_ok:0, error:"entrada no json"}.to_json

    post '/', post_not_json

    expect(last_response.body).to eq expected_json
  end

  it 'post / with debug false and lacking "template" field should return bad request status code' do
    post_json = {
      contactos: [{"mail":"juanperez@test.com"}],
      datos: {remitente: "pepe@test.com",
              asunto: "hola"}
    }.to_json

    _post(post_json)

    expect(last_response.status).to eq BAD_REQUEST_STATUS_CODE
  end

  it 'post / with debug false and lacking "template" field should return json {"msg_ok":0,"error":"json con esquema incorrecto"}' do
    post_json = {
      contactos: [{"mail":"juanperez@test.com"}],
      datos: {remitente: "pepe@test.com",
              asunto: "hola"}
    }.to_json
    expected_json = {"msg_ok":0,"error":"json con esquema incorrecto"}.to_json

    _post(post_json)

    expect(last_response.body).to eq expected_json
  end

  it 'post / with debug true and lacking "template" field should return json {"error":"json con esquema incorrecto"}' do
    post_json = {
      debug: true,
      contactos: [{"mail":"juanperez@test.com"}],
      datos: {remitente: "pepe@test.com",
              asunto: "hola"}
    }.to_json
    expected_json = {"error":"json con esquema incorrecto"}.to_json

    _post(post_json)

    expect(last_response.body).to eq expected_json
  end

  it 'post / with two valid contacts should return { msg_ok: 2 }' do
    post_json = {
      debug: false,
      template: "Hola Mundo!",
      contactos: [{"mail":"juanperez@test.com"},
                  {"mail":"juanperez2@test.com"}],
      datos: {remitente: "pepe@test.com",
              asunto: "hola"}
    }.to_json
    expected_json = { "msg_ok": 2 }.to_json

    _post(post_json)

    expect(last_response).to be_ok
    expect(last_response.body).to eq expected_json
  end

  it 'post / with one valid contact and one invalid contact should return { msg_ok: 1 } and
      return 202' do
    post_json = {
      debug: false,
      template: "Hola Mundo!",
      contactos: [{"mail":"juanperez@test.com"},
                  {"mail":"juanperez2"}],
      datos: {remitente: "pepe@test.com",
              asunto: "hola"}
    }.to_json
    expected_json = { "msg_ok": 1 }.to_json

    _post(post_json)

    expect(last_response.status).to eq ACCEPTED_STATUS_CODE
    expect(last_response.body).to eq expected_json
  end

  it 'post / with static variable in contacts should return {msg_ok: 1}' do
    post_json = {
      debug: false,
      template: "Hola <nombre>!",
      contactos: [{"mail":"juanperez@test.com",
                   "nombre": "Juan"}],
      datos: {remitente: "pepe@test.com",
              asunto: "hola"}
    }.to_json

    expected_json = { "msg_ok": 1 }.to_json

    _post(post_json)

    expect(last_response).to be_ok
    expect(last_response.body).to eq expected_json
  end

  it 'post / with debug and static variable in contacts should return {"text":"Hola Juan"}' do
    post_json = {
      debug: true,
      template: "Hola <nombre>",
      contactos: [{"mail":"juanperez@test.com",
                   "nombre": "Juan"}],
      datos: {remitente: "pepe@test.com",
              asunto: "hola"}
    }.to_json
    expected_json = { "text":"Hola Juan" }.to_json

    _post(post_json)

    expect(last_response).to be_ok
    expect(last_response.body).to eq expected_json
  end

  it 'post / with debug and static variable in contacts should return {"text":"Comi papas"}' do
    post_json = {
      debug: true,
      template: "Comi <comida>",
      contactos: [{"mail":"juanperez@test.com",
                   "comida": "papas"}],
      datos: {remitente: "pepe@test.com",
              asunto: "hola"}
    }.to_json
    expected_json = { "text":"Comi papas" }.to_json

    _post(post_json)

    expect(last_response).to be_ok
    expect(last_response.body).to eq expected_json
  end

  it 'post / with debug and static variable not found should return bad request status code' do
    post_json = {
      debug: true,
      template: "Comi <comida>",
      contactos: [{"mail":"juanperez@test.com"}],
      datos: {remitente: "pepe@test.com",
              asunto: "hola"}
    }.to_json

    _post(post_json)

    expect(last_response.status).to eq BAD_REQUEST_STATUS_CODE
  end

  it 'post / with debug and static variable "nombre" not found should return {"error":"falta etiqueta nombre"} as body' do
    post_json = {
      debug: true,
      template: "Hola <nombre>",
      contactos: [{"mail":"juanperez@test.com"}],
      datos: {remitente: "pepe@test.com",
              asunto: "hola"}
    }.to_json
    expected_json = {"error":"falta etiqueta nombre"}.to_json

    _post(post_json)

    expect(last_response.body).to eq expected_json
  end

  it 'post / with debug and static variable "comida" not found should return {"error":"falta etiqueta comida"} as body' do
    post_json = {
      debug: true,
      template: "Comi <comida>",
      contactos: [{"mail":"juanperez@test.com"}],
      datos: {remitente: "pepe@test.com",
              asunto: "hola"}
    }.to_json
    expected_json = {"error":"falta etiqueta comida"}.to_json

    _post(post_json)

    expect(last_response.body).to eq expected_json
  end

  it 'post / with debug and one valid mail with static variable and one invalid mail with
      static variable should return {"text":"Hola Juan"}' do
    post_json = {
      debug: true,
      template: "Hola <nombre>",
      contactos: [{"mail":"juanperez@test.com", "nombre":"Juan"},
                  {"mail":"falta_nombre@test.com"},
                  {"mail":"zaraza"}],
      datos: {remitente: "pepe@test.com",
              asunto: "Saludo"}
    }.to_json
    expected_json = {"text":"Hola Juan"}.to_json

    _post(post_json)

    expect(last_response.body).to eq expected_json
  end

  it 'post / with debug=false and static variable found in data should return {"text": "Hola Juan"}' do
    post_json = {
      debug: true,
      template: "Hola <nombre>",
      contactos: [{"mail":"juanperez@test.com"}],
      datos: {remitente: "pepe@test.com",
              asunto: "hola",
              nombre: "Juan"}
    }.to_json
    expected_json = { "text":"Hola Juan" }.to_json

    _post(post_json)

    expect(last_response.body).to eq expected_json
    expect(last_response).to be_ok
  end

  it 'post / with debug=false and several problems should return {"msg_ok": 0}' do
    post_json = {
      debug: false,
      template: "Hola <nombre>",
      contactos: [{"mail":"juanperez@test.com"},{"mail":"zaraza","nombre":"zaraza"}],
      datos: {"asunto":"Saludo","remitente":"pepe@test.com"}
    }.to_json
    expected_json = { "msg_ok": 0 }.to_json

    _post(post_json)

    expect(last_response.body).to eq expected_json
    expect(last_response.status).to eq BAD_REQUEST_STATUS_CODE
  end

  it 'post / with debug=true and empty tag should return {"text": "Hola pibe"}' do
    post_json = { "debug":true,
                  "template":"Hola <empty(nombre%%pibe)>",
                  "contactos":[{"mail":"juanperez@test.com"}],
                  "datos": { "asunto":"Saludo",
                             "remitente": "pepe@test.com"}}.to_json
    expected_json = { "text": "Hola pibe" }.to_json

    _post(post_json)

    expect(last_response.body).to eq expected_json
  end

  it 'post / with debug=true, empty tag and static tag should return {"text": "Hola Juan pibe}' do
    post_json = { "debug":true,
                  "template":"Hola <nombre> <empty(apodo%%pibe)>",
                  "contactos":[{"mail":"juanperez@test.com", "nombre": "Juan"}],
                  "datos": { "asunto":"Saludo",
                             "remitente": "pepe@test.com"}}.to_json
    expected_json = { "text": "Hola Juan pibe" }.to_json

    _post(post_json)

    expect(last_response.body).to eq expected_json
  end

  it 'post / with debug=true, empty tag with tag in contactos should return {"text": "Hola Juan}' do
    post_json = { "debug":true,
                  "template":"Hola <empty(nombre%%pibe)>",
                  "contactos":[{"mail":"juanperez@test.com", "nombre": "Juan"}],
                  "datos": { "asunto":"Saludo",
                             "remitente": "pepe@test.com"}}.to_json
    expected_json = { "text": "Hola Juan" }.to_json

    _post(post_json)

    expect(last_response.body).to eq expected_json
  end

  it 'post / with debug=true and template "hola <today(%d%m%Y)>" should return 200 and
    {"text": "hola 01012018"}' do
    post_json = { "debug":true,
                "template":"hola <today(%d%m%Y)>",
                "contactos":[{"mail":"juanperez@test.com", "nombre": "Juan"}],
                "datos": { "asunto":"Saludo",
                           "remitente": "pepe@test.com"}}.to_json

    expected_json = { "text": "hola 01012018" }.to_json

    _post(post_json)

    expect(last_response).to be_ok
    expect(last_response.body).to eq expected_json
  end

  it 'post / with debug=true and embed online file with "hola\n" in "asd <embed...>"
  should return 200 and {"text": "hola 01012018"}' do
    embedding_url = "https://raw.githubusercontent.com/nicopaez/ejemplo/master/hola.txt"
    post_json = { "debug":true,
                  "template":"asd <embed(%s)>" % embedding_url,
                  "contactos":[{"mail":"juanperez@test.com", "nombre": "Juan"}],
                  "datos": { "asunto":"Saludo",
                             "remitente": "pepe@test.com"}}.to_json

    expected_json = { "text": "asd "+@fake_embedding }.to_json

    _post(post_json)

    expect(last_response).to be_ok
    expect(last_response.body).to eq expected_json
  end

  it 'post / with debug=true and embed a page that responds with error when requested'+
  'should return 400 and {"error": "Error al procesar URL"}' do
    embedding_url = "https://raw.githubusercontent.com/nicopaez/ejemplo/master/hola.txt"
    post_json = { "debug":true,
                  "template":"asd <embed(%s)>" % embedding_url,
                  "contactos":[{"mail":"juanperez@test.com", "nombre": "Juan"}],
                  "datos": { "asunto":"Saludo",
                             "remitente": "pepe@test.com"}}.to_json

    TemplateInterpreter.any_instance.stub(:embed).and_raise(OpenURI::HTTPError.new("hola","someIO"))

    expected_json = { "error": "Error al procesar URL" }.to_json

    _post(post_json)

    expect(last_response.status).to eq BAD_REQUEST_STATUS_CODE
    expect(last_response.body).to eq expected_json
  end

  it 'post / with debug=true and "Hola en ingles es <translate(EN%%hola)>" should return 200 and
     {"text": "Hola en ingles es hello"}"' do
    post_json = { "debug":true,
                  "template":"Hola en ingles es <translate(EN%%hola)>",
                  "contactos":[{"mail":"juanperez@test.com", "nombre": "Juan"}],
                  "datos": { "asunto":"Saludo",
                             "remitente": "pepe@test.com"}}.to_json

    expected_json = { "text": "Hola en ingles es "+ @fake_translate }.to_json

    _post(post_json)

    expect(last_response).to be_ok
    expect(last_response.body).to eq expected_json
  end

  it 'post / with debug=true and the translation returns an error, then it should return 400 and
      {"error": "Error al intentar traducir"}' do
    post_json = { "debug":true,
                  "template": "Hola en ingles es <translate(EN%%hola)>",
                  "contactos":[{"mail":"juanperez@test.com", "nombre": "Juan"}],
                  "datos": { "asunto":"Saludo",
                             "remitente": "pepe@test.com"}}.to_json

    TemplateInterpreter.any_instance.stub(:translate).and_raise(EasyTranslate::EasyTranslateException.new())

    expected_json = { "error": "Error al intentar traducir" }.to_json

    _post(post_json)

    expect(last_response.status).to eq BAD_REQUEST_STATUS_CODE
    expect(last_response.body).to eq expected_json
  end

  it 'post / with debug=true and xpath for title of job vacancy URL should return 200 and '+
  '{"text":"El titulo de jobvacancy es Job Vacancy Board - find the best jobs"}' do
    post_json = { "debug":true,
                  "template": "El titulo de jobvacancy es <xpath(http://www.jobvacancy.com.ar/%%//title/text())>",
                  "contactos":[{"mail":"juanperez@test.com", "nombre": "Juan"}],
                  "datos": { "asunto":"Saludo",
                             "remitente": "pepe@test.com"}}.to_json

    expected_json = { "text": "El titulo de jobvacancy es Job Vacancy Board - find the best jobs" }.to_json

    _post(post_json)

    expect(last_response).to be_ok
    expect(last_response.body).to eq expected_json
  end

  it 'post / with debug=true and wrong xpath for job vacancy URL should return 400 '+
  'and {"error": "expresion xpath invalida"}' do
    post_json = { "debug":true,
                  "template": "El titulo de jobvacancy es <xpath(http://www.jobvacancy.com.ar/%%//sds/tedsxt())>",
                  "contactos":[{"mail":"juanperez@test.com", "nombre": "Juan"}],
                  "datos": { "asunto":"Saludo",
                             "remitente": "pepe@test.com"}}.to_json

    expected_json = { "error": "expresion xpath invalida" }.to_json

    _post(post_json)

    expect(last_response.status).to eq BAD_REQUEST_STATUS_CODE
    expect(last_response.body).to eq expected_json
  end

  it 'post / with debug=true and title of job vacancy URL should return 200 and '+
  '{"text":"El titulo de jobvacancy es Job Vacancy Board - find the best jobs"}' do
    post_json = { "debug":true,
                  "template": "El titulo de jobvacancy es <title(http://www.jobvacancy.com.ar/)>",
                  "contactos":[{"mail":"juanperez@test.com", "nombre": "Juan"}],
                  "datos": { "asunto":"Saludo",
                             "remitente": "pepe@test.com"}}.to_json

    expected_json = { "text": "El titulo de jobvacancy es Job Vacancy Board - find the best jobs" }.to_json

    _post(post_json)

    expect(last_response).to be_ok
    expect(last_response.body).to eq expected_json
  end

  it 'post / with debug=true, "template":"Hey <if(pais = argentina%%hola che%%hello)> <nombre>" and
      pais is canada should return {"text": "Hey hello Juan"}' do
    post_json = { "debug": true,
                  "template": "Hey <if(pais = argentina%%hola che%%hello)> <nombre>",
                  "contactos": [{"mail":"juanperez@test.com", "nombre":"Juan"}],
                  "datos": {"asunto":"Saludo","remitente":"pepe@test.com", "pais":"canada"}}.to_json
    expected_json = { "text": "Hey hello Juan" }.to_json

    _post(post_json)

    expect(last_response.body).to eq expected_json
    expect(last_response).to be_ok
  end

  it 'post / with debug=true, "template":"Hey <if(pais = argentina%%hola che%%hello)> <nombre>" and
      pais is argentina should return {"text": "Hey hola che Juan"}' do
    post_json = { "debug": true,
                  "template": "Hey <if(pais = argentina%%hola che%%hello)> <nombre>",
                  "contactos": [{"mail":"juanperez@test.com", "nombre":"Juan"}],
                  "datos": {"asunto":"Saludo","remitente":"pepe@test.com", "pais":"argentina"}}.to_json
    expected_json = { "text": "Hey hola che Juan" }.to_json

    _post(post_json)

    expect(last_response.body).to eq expected_json
    expect(last_response).to be_ok
  end

  it 'post / with debug=true with composite tag should return {"text": "Hola 9 de julio"}' do
    post_json = { "debug":true,
                  "template":"Hola <empty(feriado%%<if(pais = argentina%%siempre%%nunca)>)>",
                  "contactos":[{"mail":"juanperez@test.com", "nombre":"Juan"}],
                  "datos": {"asunto":"caso e9","remitente":"pepe@test.com",
                            "pais":"argentina", "feriado":"9 de julio"}}.to_json
    expected_json = { "text": "Hola 9 de julio" }.to_json

    _post(post_json)

    expect(last_response.body).to eq expected_json
    expect(last_response).to be_ok
  end
end
