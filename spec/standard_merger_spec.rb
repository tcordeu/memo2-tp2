require_relative('../model/standard_merger.rb')

describe 'Standard Merger' do
  let(:merger) { StandardMerger.new }

  def _make_mail_info(base_template, sendee_info, sender_info)
    Hash[template: base_template,
         sender_info: sender_info,
         sendee_info: sendee_info,
         mail: sendee_info[:mail]]
  end

  before(:each) do
    merger.stub(:send_mail).and_return(nil)
  end

  it 'merge of a list with one ok mail info with base template "hola mundo" should return'+
  ' 1 mail and no errors' do
    in_base_template =  "hola mundo"
    in_sendee_info =  { mail: 'juanperez@test.com' }
    in_sender_info = { asunto: 'Saludo', remitente: 'pepe@test.com' }
    in_mail_info = [_make_mail_info(in_base_template, in_sendee_info, in_sender_info)]

    expected_mails_size = 1

    resulting_mails, resulting_error_list = merger.merge(in_mail_info)
    expect(resulting_mails.size).to eq expected_mails_size
    expect(resulting_error_list).to be_empty
  end

  it 'merge of a list with two ok mail infos should return 2 mails and no errors' do
    in_base_template =  "hola mundo"
    in_sendee_info =  { mail: 'juanperez@test.com' }
    in_sender_info = { asunto: 'Saludo', remitente: 'pepe@test.com' }
    in_mail_info = [_make_mail_info(in_base_template, in_sendee_info, in_sender_info),
                    _make_mail_info(in_base_template, in_sendee_info, in_sender_info)]

    expected_mails_size = 2

    resulting_mails, resulting_error_list = merger.merge(in_mail_info)
    expect(resulting_mails.size).to eq expected_mails_size
    expect(resulting_error_list).to be_empty
  end

  it 'merge of a list with one ok and two wrong mail infos should return 1 mail and'+
  ' 2 errors' do
    in_base_template =  "hola mundo"
    in_sendee_info =  { mail: 'juanperez@test.com' }
    in_sender_info = { asunto: 'Saludo', remitente: 'pepe@test.com' }
    in_sendee_info2 = { mail: 'zaraza'  }
    in_base_template2 =  "hola <asereje>"
    in_mail_info = [_make_mail_info(in_base_template, in_sendee_info, in_sender_info),
                    _make_mail_info(in_base_template, in_sendee_info2, in_sender_info),
                    _make_mail_info(in_base_template2, in_sendee_info, in_sender_info)]

    expected_mails_size = 1
    expected_error_list_size = 2

    resulting_mails, resulting_error_list = merger.merge(in_mail_info)
    expect(resulting_mails.size).to eq expected_mails_size
    expect(resulting_error_list.size).to eq expected_error_list_size
  end

  it 'merge of a list with 2 wrong mail infos should return no mails and 2 errors' do
    in_base_template =  "hola mundo"
    in_sendee_info =  { mail: 'juanperez@test.com' }
    in_sender_info = { asunto: 'Saludo', remitente: 'pepe@test.com' }
    in_sendee_info2 = { mail: 'zaraza'  }
    in_base_template2 =  "hola <asereje>"
    in_mail_info = [_make_mail_info(in_base_template2, in_sendee_info, in_sender_info),
                    _make_mail_info(in_base_template, in_sendee_info2, in_sender_info)]

    expected_error_list_size = 2

    resulting_mails, resulting_error_list = merger.merge(in_mail_info)
    expect(resulting_mails).to be_empty
    expect(resulting_error_list.size).to eq expected_error_list_size
  end
end
